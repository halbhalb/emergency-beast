#!/usr/bin/env bash
apt-get update
apt-get install -q -y nano
apt-get install -q -y apache2
apt-get install -q -y libapache2-mod-php5
apt-get install -q -y python-software-properties
apt-get install -q -y curl
export DEBIAN_FRONTEND=noninteractive
apt-get install -q -y mysql-server-5.5
apt-get install -q -y git-core
apt-get install -q -y php5-mcrypt
apt-get install -q -y php5-mysql
apt-get install -q -y php5-curl
curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

#apache set up with a default vhost in /vagrant/public
rm -rf /var/www
ln -fs /vagrant /var/www
# Add ServerName to httpd.conf
echo "ServerName localhost" > /etc/apache2/httpd.conf
# Setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
  DocumentRoot "/vagrant/public"
  ServerName localhost
  <Directory "/vagrant/public">
    AllowOverride All
  </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-enabled/000-default
a2enmod rewrite
service apache2 restart

cd /var/www
composer install --dev
echo "CREATE DATABASE IF NOT EXISTS seaslugs" | mysql
echo "CREATE USER 'seaslugs'@'localhost' IDENTIFIED BY ''" | mysql
echo "GRANT ALL PRIVILEGES ON seaslugs.* TO 'seaslugs'@'localhost' IDENTIFIED BY 'seaslugs'" | mysql
# Run artisan migrate to setup the database and schema, then seed it
php artisan migrate --env=development
php artisan db:seed --env=development