<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBeastsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('beasts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('beast_id');
			$table->string('gallery_id');
			$table->string('image');
			$table->string('caption', 2000);
			$table->boolean('tweeted');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('beasts');
	}

}
