<?php

return array(

	'connections' => array(

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '10.0.0.2',
			'database'  => 'emergencybeast',
			'username'  => 'emergencybeast',
			'password'  => 'emergencybeast',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		)
	),
);
