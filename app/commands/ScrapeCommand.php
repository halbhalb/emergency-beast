<?php

use Illuminate\Console\Command;
use Illuminate\Filesystem\FileNotFoundException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ScrapeCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'beasts:scrape';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Insert all beasts into the table';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$galleries = $this->findGalleries();
		foreach ($galleries as $gallery) {
			try {
				$beasts = $this->readGallery($gallery['index'], $gallery['file']);
				foreach ($beasts as $beast)  {
					$this->saveBeast($beast);
				}
			} catch (FileNotFoundException $e) {
			}
		}
		exit;
	}

	protected function saveBeast($beast)
	{
		$existing = Beast::where('beast_id', '=', $beast['beast'])->count();
		if ($existing) {
			return;
		}
		$dbBeast = new Beast;
		$dbBeast->gallery_id = $beast['gallery'];
		$dbBeast->beast_id = $beast['beast'];
		$dbBeast->caption = $beast['caption'];
		$dbBeast->image = $beast['file'];
		$dbBeast->save();
	}

	protected function findGalleries() 
	{
		$galleries = [];
		$files = File::files($this->getBestiaryGalleriesPath());
		foreach ($files as $gallery) {
			$basename = basename($gallery);
			$matches = null;
			if (preg_match("/^beastgallery(\d+)\.htm$/", $basename, $matches)) {
				$galleries[] = ['index'=>$matches[1], 'file'=>$gallery];
			}
		} 
		return $galleries;
	}

	protected function readGallery($gallery, $file) 
	{
		$contents = File::get($file);
		$galleryDir = dirname($file);
		$beasts = [];
		$result = preg_match_all('/var imgRef(?<index>\d+) = \'(?<file>[^;\']+)\';\s+var imgCaption(\d+) =(?<caption>(\s*\'[^\']+\' \+)*(\s*\'[^\']+\';))/', $contents, $matches);
		if ($result) {
			$indexParts = $matches['index'];
			$fileParts = $matches['file'];
			$captionParts = $matches['caption'];
			for ($i=0; $i<count($fileParts); $i++) {
				$index = $indexParts[$i];
				$filePart = $fileParts[$i];
				$fileMatches = [];
				preg_match('/\.\.\/beastimage\/img\d+\.jpg/', $filePart, $fileMatches);
				$file = realpath($galleryDir.'/'.$fileMatches[0]);
				$captionPart =  preg_replace('/\' \+\s*\'/', '', $captionParts[$i]);
				$captionMatches = [];
				preg_match('/<p>(.+)<\/p>\'/', $captionPart, $captionMatches);
				if (count($captionMatches)>1) {
					$caption = html_entity_decode(strip_tags($captionMatches[1]));
					$beast = [
						'gallery' => $gallery,
						'beast' => $index,
						'path' => $file,
						'file' => basename($file),
						'caption' => $caption,
					];
					$beasts[] = $beast;
				} else {
					print_r($captionPart);
					die(__FILE__.'.'.__LINE__);
				}
			}
		}
		return $beasts;
	}

	protected function getBestiaryPath()
	{
		return storage_path().'/media/bestiary.ca';
	}

	protected function getBestiaryGalleriesPath() 
	{
		return $this->getBestiaryPath().'/beasts';	
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
