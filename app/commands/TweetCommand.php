<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TweetCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'beasts:tweet';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send a tweet with a random beast.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$beast = Beast::findRandomUntweeted();
		if ($beast) {
			echo($beast->beast_id."\n");
			$this->tweetBeast($beast);
			$beast->tweeted=1;
			$beast->save();
		}
		exit;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

	protected function tweetBeast($beast) 
	{
		$imageName = $this->getBestiaryImagesPath().'/'.$beast->image;
		$image = file_get_contents($imageName);
		$galleryUrl = "http://bestiary.ca/beasts/beastgallery".$beast->gallery_id.".htm";
		$maxLength = 138 - strlen($galleryUrl);
		if (strlen($beast->caption)<=$maxLength) {
			$tweetText = $beast->caption;
		} else {
			$tweetText = substr($beast->caption, 0, $maxLength-3).'...';
		}
		$tweetText .= ' '.$galleryUrl;
		echo($tweetText."\n");
		$result = json_decode(Twitter::postTweetMedia(
	        array(
	            'status' => $tweetText,
	            'media[]' =>  [$image],
	            'format' => 'json'
	        )
		));
		if (property_exists($result, 'errors')) {
			throw new Exception($result->errors[0]->message);
		}
		return true;

	}

	protected function getBestiaryPath()
	{
		return storage_path().'/media/bestiary.ca';
	}

	protected function getBestiaryImagesPath() 
	{
		return $this->getBestiaryPath().'/beastimage';	
	}

}
