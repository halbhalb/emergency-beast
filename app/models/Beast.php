<?php

class Beast extends \Eloquent {
	protected $guarded = ['id'];

	public static function findRandomUntweeted() {
		$beast = self::where('tweeted', '=', 0)->orderByRaw('RAND()')->first();
		return $beast;
	}
}